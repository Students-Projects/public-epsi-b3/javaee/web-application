package TEST;

import dao.EvenementDao;
import dao.ReservationDao;
import database.orm.Evenement;
import database.orm.Personne;
import database.orm.Reservation;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.spi.CalendarDataProvider;

public class JavaOrmTP_1_test {
    public static void main(String[] args)
    {

        Personne Keel = new Personne();
        Keel.setUsername("Keel");
        Personne Usakin = new Personne();
        Usakin.setUsername("Usakin");
        Personne Xelek = new Personne();
        Xelek.setUsername("Xelek");
        Personne Soruka = new Personne();
        Soruka.setUsername("Soruka");

        Evenement animasia = new Evenement();
        animasia.setCode("HahyufoeJie4dae9zaezachesh2asei1ahthurai4IevoSha1ai5peibai5eJao7Iiw2quuPh5pai9meeghiekaijeekie0ef6hoo3xieyoo0PhohbooquaPeeShaeki");
        animasia.setEvent_date(new GregorianCalendar(2019,Calendar.APRIL,27));
        animasia.setName("Animasia Le Haillan");

        List<Personne> pers = new ArrayList<>();
        pers.add(Keel);
        pers.add(Usakin);
        pers.add(Xelek);

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("orm-jee-database");
        EntityManager entityManager = emf.createEntityManager();

        boolean transactionOk = false;

        entityManager.getTransaction().begin();
        try {
            entityManager.persist(Keel);
            entityManager.persist(Usakin);
            entityManager.persist(Xelek);
            entityManager.persist(Soruka);
            transactionOk = true;
        } finally {
            if (transactionOk) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        }

        try
        {
            EvenementDao.creer(animasia);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        Evenement myev = EvenementDao.get("HahyufoeJie4dae9zaezachesh2asei1ahthurai4IevoSha1ai5peibai5eJao7Iiw2quuPh5pai9meeghiekaijeekie0ef6hoo3xieyoo0PhohbooquaPeeShaeki");
        System.out.println(myev.getName());

        try
        {
            Reservation maReservtion = ReservationDao.reserver(animasia.getCode(), pers);
            System.out.println(maReservtion.getReservation_date()+" : "+"Réservation pour "+maReservtion.getPersonnes().size()+" personnes pour l'évènement "+maReservtion.getEvenement().getName());
            ReservationDao.supprimer(maReservtion);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }


        System.out.println(EvenementDao.getNbPersonnes(animasia.getCode())+" Réservation pour l'évènement "+animasia.getName());


    }
}
