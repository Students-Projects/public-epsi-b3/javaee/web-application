package database.orm;

import javax.persistence.*;
import java.util.Calendar;
import java.util.List;

@Entity
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Calendar reservation_date;

    @ManyToOne
    @JoinColumn(name = "event_id")
    private Evenement evenement;

    @ManyToMany
    @JoinTable(
            name = "PersonneReservation",
            joinColumns = @JoinColumn(name = "id"),
            inverseJoinColumns = @JoinColumn(name = "Personne_id")
    )
    private List<Personne> Personnes;

    public Long getId() {
        return id;
    }

    public Calendar getReservation_date() {
        return reservation_date;
    }

    public void setReservation_date(Calendar date) {
        this.reservation_date = date;
    }

    public Evenement getEvenement() {
        return evenement;
    }

    public void setEvenement(Evenement evenement) {
        this.evenement = evenement;
    }

    public List<Personne> getPersonnes() {
        return Personnes;
    }

    public void setPersonnes(List<Personne> personnes) {
        Personnes = personnes;
    }
}
