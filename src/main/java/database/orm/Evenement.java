package database.orm;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


@Entity
@NamedQueries({
        @NamedQuery(name="findByEventName", query="select i from Evenement i where i.name = :nom"),
        @NamedQuery(name="findByEventCode", query="select i from Evenement i where i.code = :code")
})
public class Evenement {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Basic
    @Column(length = 128, nullable = false, unique = true)
    private String code;

    @Basic
    @Column(length = 64, nullable = false)
    private String name;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Calendar event_date;

    @OneToMany
    private List<Reservation> reservations = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getEvent_date() {
        return event_date;
    }

    public void setEvent_date(Calendar date) {
        this.event_date = date;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }
}
