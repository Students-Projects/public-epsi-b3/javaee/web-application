package dao;

import database.orm.Evenement;
import database.orm.Reservation;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EvenementDao {

    public static void creer(Evenement e) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("orm-jee-database");
        EntityManager entityManager = emf.createEntityManager();

        boolean transactionOk = false;

        entityManager.getTransaction().begin();
        try {
            entityManager.persist(e);
            transactionOk = true;
        } finally {
            if (transactionOk) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        }
    }

    public static Evenement get(String code) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("orm-jee-database");
        EntityManager entityManager = emf.createEntityManager();

        return entityManager.createNamedQuery("findByEventCode", Evenement.class).setParameter("code", code).getSingleResult();
    }

    public static Integer getNbPersonnes(String code) {
        Evenement e = EvenementDao.get(code);
        int nb = 0;

        for (Reservation r:e.getReservations())
        {
            nb+=r.getPersonnes().size();
        }

        return nb;
    }
}
