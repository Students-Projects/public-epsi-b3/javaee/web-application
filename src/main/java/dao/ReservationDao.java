package dao;

import database.orm.Personne;
import database.orm.Reservation;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Calendar;
import java.util.List;

public class ReservationDao {

    public static Reservation reserver(String codeEvenement, List<Personne> personnes)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("orm-jee-database");
        EntityManager entityManager = emf.createEntityManager();
        boolean transactionOk = false;
        Reservation r = new Reservation();


        entityManager.getTransaction().begin();
        try {
            r.setPersonnes(personnes);
            r.setEvenement(EvenementDao.get(codeEvenement));
            r.setReservation_date(Calendar.getInstance());

            entityManager.persist(r);
            transactionOk = true;

        } finally {
            if (transactionOk) {
                entityManager.getTransaction().commit();
            } else {
                entityManager.getTransaction().rollback();
            }
        }

        return r;
    }

    public static void supprimer(Reservation reservation)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("orm-jee-database");
        EntityManager entityManager = emf.createEntityManager();


        boolean transactionOk = false;


        entityManager.getTransaction().begin();
        try {
            entityManager.remove(reservation);
            transactionOk = true;
        }
        finally {
            if(transactionOk) {
                entityManager.getTransaction().commit();
            }
            else {
                entityManager.getTransaction().rollback();
            }
        }

        System.out.println("Reservation "+reservation.getId()+" Correctly Supressed");

    }


}
