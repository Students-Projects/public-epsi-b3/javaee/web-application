# Projet Java EE

# Projet Java ORM

Script de création de la base de donnée : 

```mariadb
    CREATE TABLE Evenement (
        id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
        code VARCHAR(128) UNIQUE NOT NULL,
        name VARCHAR(64) NOT NULL,
        event_date DATE NOT NULL
    );

    CREATE TABLE Personne (
        id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
        username VARCHAR(64) NOT NULL UNIQUE
    );
    
    CREATE TABLE Reservation (
        id INT PRIMARY KEY  NOT NULL AUTO_INCREMENT,
        reservation_date DATE NOT NULL,
        event_id INT NOT NULL,
        CONSTRAINT FK_res_event FOREIGN KEY (event_id) REFERENCES Evenement(id)
    );
    
    CREATE TABLE PersonneReservation (
        reservation_id INT NOT NULL,
        personne_id INT NOT NULL,
        CONSTRAINT FK_res_pers FOREIGN KEY (reservation_id) REFERENCES Reservation(id),
        CONSTRAINT FK_pers_res FOREIGN KEY (personne_id) REFERENCES Personne(id),
        CONSTRAINT PK_pers_res PRIMARY KEY (reservation_id,personne_id)
                                
    );
```

